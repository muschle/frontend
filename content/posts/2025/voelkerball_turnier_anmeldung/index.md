+++ 
draft = false
date = 2025-02-28T22:00:00+02:00
title = "Völkerballturnier Anmeldung"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["home", "voelkerball"]
externalLink = "https://ohmyform.burkhalter.one/form/GVkRjB"
+++

<iframe 
    src="https://ohmyform.burkhalter.one/form/GVkRjB" 
    style="border:0px #ffffff none;" 
    name="myiFrame" 
    scrolling="no" 
    frameborder="1" 
    marginheight="0px" 
    marginwidth="0px" 
    height="1200px" 
    width="1200px" 
    allowfullscreen>
</iframe>
