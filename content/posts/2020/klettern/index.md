+++ 
draft = false
date = 2020-01-01T22:00:00+02:00
title = "Klettern"
description = "Es wahnsinns Video"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["abenteuer", "home"]
externalLink = ""
+++

<iframe 
    width="1280"
    height="720"
    src="https://www.youtube.com/embed/fgi9D0TydRY"
    title="Klettern 2020"
    frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    allowfullscreen="">
</iframe>