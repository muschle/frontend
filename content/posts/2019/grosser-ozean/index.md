+++ 
draft = false
date = 2019-07-15T22:00:00+02:00
title = "Grosser Ozean"
description = "Es wahnsinns Video"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["abenteuer", "home"]
externalLink = ""
+++

<iframe 
    width="1280" 
    height="720" 
    src="https://www.youtube.com/embed/_CsPX5JjY7o" 
    title="Grosser Ozean" 
    frameborder="0" 
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
    allowfullscreen="">
</iframe>
