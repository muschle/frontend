+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Urhell"
description = "Muschle's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++

Das aktuelle Design der Webseite ist dem Urhell gewidmet, das Lieblingsbier einer unserer Muscheln.


dark: false

carbonator : {
    "backgroundColor": "#cb9203"
}

colors: {
    "header": "#e4e4e2",
    "primary": "#f26617"
}

Raphael