+++ 
draft = false
date = 2022-03-24T22:00:00+02:00
title = "Muschlen im Sand"
description = "Ein Gedicht von Hannelore Knödler-Stojanovic, Ludwigsburg"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["home"]
externalLink = ""
+++

<blockquote>
Ein Spaziergang am Strand ist schön, <br>
da gibt’s viele Muscheln zu seh’n. <br>
Bizarre, große und ganz kleine, <br>
grob geformte und künstlerisch feine, <br>
in den Tiefen des Ozeans geboren,<br>
von den Meereswogen empor gehoben.<br><br>

Jetzt liegen sie verstreut im Sand,<br>
von den Wellen gespült an den Strand,<br>
wie von Künstlerhand erschaffen<br>
verschiedene Formen, bezaubernde Arten.<br><br>

Manchmal bildet sich Perlmutt an der Wand,<br>
in der dann eine kostbare Perle entstand,<br>
eine verführerische Träne aus dem Ozean,<br>
sie schimmert, fühlt sich glatt und edel an.<br><br>

Auch gibt’s köstliche Muscheln zum Essen,<br>
sie gehören zu den beliebten Delikatessen.<br>
Auch Krabben finden darin ihr Eigenheim,<br>
sie fühlen sich in der Muschel daheim.<br>

Ich kann mich an Muscheln nicht satt seh’n,<br>
finde sie ganz einfach umwerfend schön.<br>
Ich suche mir die Schönsten heraus<br>
und klebe Muschelbilder zu Haus.<br><br>

Schaue ich das Bild später an,<br>
gehe ich wieder den Strand entlang,<br>
die Erinnerung kehrt zu mir zurück<br>
an diesen wunderschönen Augenblick.<br>
</blockquote>

Hannelore Knödler-Stojanovic, Ludwigsburg

https://www.gedichtesammlung.net/Natur-Jahreszeiten/Gedichte-Natur/Muscheln-im-Sand/