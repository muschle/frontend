# muschle

https://m.uschle.ch

## commands

https://gohugo.io/commands/

### hugo builds your site

```
hugo
```

### A high performance webserver

```
hugo serve
```

### Create new content for your site

```
hugo new <<type>> <<location>>
```

e.g. `hugo new post /posts/2023-06-05/index.md`

### List all drafts

```
hugo list drafts
```
