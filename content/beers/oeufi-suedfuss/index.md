+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Öufi Südfuss"
description = "Mänu's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++
Das aktuelle Design der Webseite ist dem Öufi Südfuss gewidmet, das Lieblingsbier einer unserer Muscheln.



Ein goldenes Spezialbier mit Gerstenmalz aus Altreu und zwei Hopfensorten vom Schlatthof in Wolfwil.



https://www.oeufi-bier.ch

dark: false

carbonator : {
"backgroundColor": "#c6a203"
}

colors: {
"header": "#f3f0e6",
"primary": "#d80001"
}