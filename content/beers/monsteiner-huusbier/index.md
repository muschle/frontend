+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Monsteiner Huusbier"
description = "Jere's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++
Das aktuelle Design der Webseite ist dem Monsteiner Huusbier gewidmet, das Lieblingsbier einer unserer Muscheln.



Das Monsteiner Huusbier ist unsere Antwort auf deinen gradlinigen Bierdurst. Aus kristallklarem Quellwasser und Bündner Berggerste entsteht ein helles und unfiltriertes Bier, mit dem das Vollbier sein Comeback feiert.



https://monsteiner.ch/

dark: true

carbonator : {
"backgroundColor": "#5c0d00"
}

colors: {
"header": "#dcd3c4",
"primary": "#0e9575"
}

Jeremy