+++ 
draft = false
date = 2022-07-16T22:00:00+02:00
title = "Völkerballturnier 2022"
description = "Rückblick auf ein wahnsinns Völkerballturnier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["home", "voelkerball"]
externalLink = ""
+++

<iframe 
    width="1280"
    height="720"
    src="https://www.youtube.com/embed/VwSiJwJ7Aw0"
    title="Völkerballturnier 2022"
    frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    allowfullscreen="">
</iframe>
