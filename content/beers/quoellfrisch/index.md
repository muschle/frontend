+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Quöllfrisch"
description = "Simon's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++

Das aktuelle Design der Webseite ist dem Quöllfrisch gewidmet, das Lieblingsbier einer unserer Muscheln.



Das Quöllfrisch hell zeichnet sich durch seine Milde und leichte Fruchtigkeit aus. Gebraut wird es aus reinem Pilsenermalz und drei verschiedenen Hopfensorten aus Stammheim und der Hallertau.



https://www.appenzellerbier.ch/-


dark: false

carbonator : {
"backgroundColor": "#c6a203"
}

colors: {
"header": "#fefff3",
"primary": "#fe3942"
}

Simon