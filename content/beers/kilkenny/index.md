+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Kilkenny"
description = "Jonas's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++

Das aktuelle Design der Webseite ist dem Kilkenny gewidmet, das Lieblingsbier einer unserer Muscheln.



Kilkenny bezeichnete eine stärkere, für den Export produzierte Variante des in Irland verkauften roten Ales Smithwick’s. Inzwischen sind dort beide Marken erhältlich. Kilkenny ist ein rotbraunes Red Ale, das etwas milder als das dunklere Guinness-Stout ist. In Irland wird es auch als Dosenbier verkauft, das mit der Zugabe eines sogenannten Floating widget – ursprünglich entwickelt, um auch Guinness aus Dosen beim Eingießen die typische Schaumkrone zu geben – ähnlich wie vom Fass gezapft schmeckt und aussieht. Smithwick’s-Dosen besitzen dieses Widget inzwischen nicht mehr. Für den deutschen Markt wird das Bier seit 2013 von der Radeberger-Gruppe importiert, zuvor war es die Warsteiner Brauerei.



https://de.wikipedia.org

dark: false

carbonator : {
"backgroundColor": "#c6a203"
}

colors: {
"header": "#f3f0e6",
"primary": "#d80001"
}

Jonas