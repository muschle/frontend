+++ 
draft = false
date = 2025-02-27T22:00:00+02:00
title = "Völkerballturnier Regeln"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["home", "voelkerball"]
externalLink = ""
+++

## Spielregeln

- Die Teams bestehen aus 7 Spielern, 6 im Hauptfeld und 1 im Himmel
- Am Anfang jedes Spiel muss der Ball 3x vom Spielfeld in den Himmel und wieder zurückgeworfen werden, ohne dass der Ball den Boden oder das Abfangnetz berührt
- Der erste Spieler im Himmel muss zurück ins Spielfeld, sobald ein zweiter im Himmel ist.
- Ein Einzelspiel gewinnt, wer es zuerst schafft alle gegnerischen Spieler im Spielfeld zu treffen, so dass sich keine Spieler im Spielfeld befinden (alle im Himmel)
- Im Himmel muss sich immer mind. eine Person aufhalten
- Wer ein gegnerischer Spieler vom Himmel aus trifft darf zurück ins Spielfeld
- Im Huckepack kann man zurück ins eigene Spielfeld gelangen, indem durch das gegnerische Spielfeld läuft ohne dabei abgeklatscht oder mit dem Ball getroffen wird
- Wer der Ball fängt, behält sein Leben
- Mit dem Ball kann man andere Bälle abwehren
- Wer von einem Ball getroffen ist, muss in den Himmel
- Der Ball darf nicht aus dem gegnerischen Spielfeld gefischt werden
- Mit dem Ball darf man sich bewegen
- Es ist nicht notwendig zuerst den Ball zu einem Mitspieler passen, um einen gegnerischen- Spieler treffen zu können
- Kopftreffer zählen, sind aber nicht erwünscht

## Spielmodus

- Die Spieldauer ist abhängig von der anzahl Teams und beträgt etwa 10 Minuten. Falls ein Team früher gewinnt stellen sich beide Teams wieder neu auf und spielen erneut
- Wer mehr Siege in den 10 Minuten holt gewinnt, bei gleicher Anzahl Siege gewinnt das Team mit mehr Feldspieler beim Abpfiff, bei gleicher Anzahl Feldspieler ist es ein Unentschieden
- Sieg = 3 Punkt, Unentschieden = 1 Punkt, Niederlage = 0 Punkt
- Seitenwechsel nach jedem beendeten Einzelspiel
- Während einem Einzelspiel darf nicht ausgewechselt werden, dazwischen schon
- Es wird mit zwei Softbällen gespielt

- Teams werden in zwei Gruppen aufgeteilt. Alle Teams in einer Gruppe spielen gegeneinander. Nach der Gruppenphase erreichen die besten 8 in die KO-Spiele, beginnend mit den Viertelfinals, danach Halbfinale und dann grosses und kleines Finale.
- Maximal 10 Teams
 
## AGB's
- Die Versicherung gegen Unfall, Krankheit und die Haftpflicht ist Sache des Teilnehmenden.
- Recht am Bild: Mit der Anmeldung ist der/die Teilnehmende einverstanden, dass während dem Event gemachte Fotos von den Muschle für Werbezwecke verwendet werden dürfen.
