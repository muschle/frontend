+++ 
draft = false
date = 2021-07-05T22:00:00+02:00
title = "Brüssel"
description = "Es wahnsinns Video"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["abenteuer", "home"]
externalLink = ""
+++

## Liebes Tagebuch
Uns war diesen Sommer ein bisschen langweilig. Deshalb haben wir beschlossen uns auf ein Abenteuer einzulassen, den das Leben beginnt ausserhalb der Komfortzone. In unserem zweiten Wohnzimmer hatten wir noch vor eintreffen der Bestellung ein Treffer, Belgien, den dass Bier und Essen ist gut und niemand von uns war bereits dort. Das AirBnB reserviert und die Tickets bestellt gingen wir voller Vorfreude nach Hause. Ich kann es kaum erwarten! Bis dann liebes Tagebuch

## Liebes Tagebuch (Tag 1)

Heute ging die Reise endlich los. Zuerst fuhren wir mit dem Zug von Solothurn nach Genf. Dann nahmen wir den Flieger Richtung Brüssel. Mit dem Uber sind wir vom Flughafen direkt zu unserem neun Hauptquertiertier gefahren. Leider befand es sich in einer zwielichtigen Gegend, neben einem Umschlagplatz für Getränke aller Art. Der Hunger war gross, weshalb wir die Leibspeisse der Belgier verschlangen. Nach Burger und Pommes sinnten wir nach einem kühlen Bier. Goldener Platz, wie der Name schon sagt glänzte der ganze Platz in den letzten Sonnenstrahlen. 

{{<figure src="./images/golden place 2.jpeg" class="aligne-left size-medium">}}
{{<figure src="./images/golden place.jpeg" class="aligne-right size-medium">}}
<div style="clear: both">

Noch nicht sichtbar und für viele Touristen nur eine Schokoladenfigur suchten wir den Manneken Pis. Was wir schon aus der Ferne hätten sehen sollen, sahen wir erst als er uns schon fast auf den Kopf urinierte, der Manneken Pis. 

{{<figure src="./images/manneken pis.jpeg" class="aligne-left size-smaller">}}

Voller Elan kauften wir uns dann auch zwei Flaschenöffner in Form des bezaubernden urinierenden Jungen. Wir hatte jedoch noch nicht aufgegeben den unser Durst war noch lange nicht verschwunden. Die Biervielfalt in diesem Land überwältigte uns Muscheln, doch wir hilten tapfer dagegen. !

{{<figure src="./images/beers.jpeg" class="aligne-right size-smaller">}}

Bis in die tief Nacht hielten wir durch. Liebes Tagebuch heute kann und will ich nicht mehr weiter schreiben. Meine Sicht verschwimmt, die anfangs noch so geschwungenen Worte wirken nun mehr wie ein besoffenes gekrakel. Für heute wünsche ich euch eine, wenn auch kurze, erholsame Nacht. Bis morgen beim Breakefest. 

{{<figure src="./images/au passin.jpeg" class="aligne-center size-medium">}}

## Liebes Tagebuch (Tag 2)

Ein unverkenliches Geräusch weckte mich. Das ploppen eines grossen belgischen Bier, nicht zu verwechseln mit dem «normalen Champagnerflaschen ploppen». Ausgetrunken das achsoköstliche Morgenbier, machten wir uns auf den weg zum Breakfest. Die ungeschönte Wahrheit das essen ist und war super. Pancakes für den Gourmet unter uns und Bagels mit Speck, Rührei und diversen Gewürzen. 

Gut verpflegt machten wir uns auf, Kunst aud dem Jahre 1958 zu erfahren. Das Atomium, eine 102 Meter grosse Nachbildung eines Atom, voll zugänglich, Silberkugeln mit einem Durchmesser von 18 unglaublichen Metern, durch Rolltreppen verbundes Kunstwerk. 

{{<figure src="./images/atomium.jpeg" class="aligne-right size-smaller">}}

Über eine unscheinbare Rolltreppe begaben wir uns in das Universum. In einer der Kugeln war eine unbeschreibliche mit dem Universum vergleichbare Lichtinstallation dargestellt worden. Unter uns, ein jedes Raver Herz, würde diesen Ort als perfekten, optimalen Austragungsort des jahrzehnte Raves definieren. Über eine immer noch mit faszinierenden Lichtinstallationen umgebene Rolltreppe verliessen wir das Atomium. 

{{<figure src="./images/atomium 2.jpeg" class="aligne-left size-smaller">}}

Zweimal umgefallen hatten mir den nächsten Kulturellen Höhepunkt schon vor uns. 

Europa miniature, die Abbildung ganz Europas, auf für uns aufblasbarem Platz faszinierte uns sehr. Man stelle sich vor ein Biervertriebsweg miniature, vielleicht eine idee für die Muscheln. 

<br>
{{<figure src="./images/umbrellas.jpeg" class="aligne-center size-small">}}

Ganz dem Vereinsziel verschrieben förderten wir den Bierkonsum an diesem Abend. An diesem Punkt MUSS ich das Au Bassin erwähnen, weder zu familiär noch zu hochgestochen überzeugte uns das Au Passin mit einer schönen mittelmässigkeit. Nicht desto trotz müssen auch wir, die Muscheln, irgendeinmal zu Bette kriechen. Nute Gacht Tiebes Lagebuch.

## Liebes Tagebuch (Tag 3)

Der zum Standard gewordenen Plopp Wecker ertönte auch heute. Dennoch war etwas anders an diesem Morgen. Wir sahen die Sonne schon, auf unserer Seite! Das konnte nicht die Morgen Sonne sein, oh Schreck es war die Mittags Sonne. Mit schwerem Kopf gingen wir in froher Erwartung richtung Woodpecker, unsere «z`morge Beiz». Die Bedienung war schnell bei uns und fragte nach unseren Essens wünschen. Fröhlich gaben wir unsere standart Bestellung durch. Als Antwort bekamen wir die Mittagsspeisekarte. Verwundert schauten wir auf die Uhr schon 12:30 wie lange hatten wir an unserem Morgen Bier! 

Leicht angeheitert schlenderten wir ohne richtiges Ziel durch einen der vielen Stadtteile Brüssels. Verschiedenste wichtige Häuser säumten unseren weg bis wir einen gemütlichen Park fanden in welchem wir unseren Durst stillen konnten. 

Aber nun zum Highlight des Tages, der Besuch der Berühmten Delirium Brauerei/Bar/Festlokal. 

{{<figure src="./images/drinks.jpeg" class="aligne-left size-small">}}

Voller Vorfreude setzten wir uns an Tisch und wollten bestellen. Eine der Muscheln hatte die unsinnige Idee Gueuze also ein Sauerbier, zu bestellen Einstimmig abgelehnt widmeten wir uns der exotischen Bierspezialität Kaktusbier. Während wir ein bisschen überwältigt von der schieren Anzahl waren genossen wir das knallgrüne Katusbier und natürlich auch diverse andere Biere. Am Ende des Abend wurde sogar das Gueuze wider einmal von einer der Muscheln bestellt. Kurzer Exkurs.

Gueuze, nach unseren Vorstellung Göss ausgesprochen beschrieb die mehr zahl der Muscheln mit dem laut bööh dem Würgelaut entsprechend des wieder hervorbringen eines Bieres.

Krönender Abschluss des Abends ,auch wenn durch den Schleier des erarbeiteten Delirium getrübt, war die ruckartige Rückgabe des Bieres an Mutter Erde. Die Geueze Muschel entleerte professionell den kompletten Mageninhalt an einen der schönen Bäume. Ehrlich gesagt kann ich ab hier nichts mehr erzählen denn meine Nächste Erinnerung findet ihr in meinem Eintrag für Morgen.

## Liebes Tagebuch (Tag 4)

Plopp, der letzte Tag hier in Belgien hat uns bereits eingeholt. Nach der Stärkung im Woodpeckers hatten wir eigentlich nur noch ein Reiseziel abzuhaken, eine belgische Waffel zu geniessen. Da jedoch noch keiner hungrig war flanierten wir ein wenig durch Brüssel und genossen die Zeit die uns noch blieb.

Immer noch nicht Hungrig aber unserer Aufgabe und Verantwortung bewusst bestellten wir uns dann doch noch eine Waffel. Fazit Waffeln sind toll aber hunger sollte man schon auch haben. 

Auf dem Rückweg zur Unterkunft wurden wir plötzlich Zeugen eines wahren Specktakels. Ein kleiner Junge stand im Park und nur Sekundenbruchteile später hielt er eine Taube in der Hand. Der Taubenjäger verfolgte anschliessend mit der Taube in der Hand andere Tauben. Was für eine Legende 🤯 

Zurück in der Unterkunft hatten wir noch ein wenig Zeit zu überbrücken, bis wir sie abgeben und zum Flughafen eilen mussten. Diese Zeit haben wir natürlich Optimal genutzt und und sofort den Aromaluftbefeuchter geschnappt. Unsere Köpfe quollen über vor Ideen. Aroma um Aroma wurde eingefüllt auch die idee das Wasser auszutauschen kam uns doch wir besannen uns rechtzeitig (die Vermieterin kam). Mit dem Gedanken bald unser Heimweh nach dem Müürli zu stillen begaben wir uns zum Flughafen. Doch auch hier sollte die Reise nicht ruhig enden. Zwei Muscheln hatten im Anfall purer Intelligenz kein Zertifikat Organisiert und da EasyJet es sehr genau nahmen mit den Regeln hier es beim Boarding hier kommst du nid rein. Okey zurück testen warten und das war der Flug schon weg. Paar weise ging es also zurück in die Schweiz und dort warteten auch schon die anderen Muscheln mit einem Bier auf uns. 

Ich möchte diesen letzten Eintrag so beenden wie ein gross teil der Tage in Brüssel begonnen hat mit dem tollen Geräusch Plopp dieses Mal in am Müürli denn am schönsten ist es doch Zuhause.
