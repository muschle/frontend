+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Duchesse de Bourgogne"
description = "Paco's lieblings Bier"
slug = ""
authors = ["Paco Burkhalter"]
categories = ["beer"]
externalLink = ""
+++

Das aktuelle Design der Webseite ist dem Duchesse de Bourgogne gewidmet, das Lieblingsbier einer unserer Muscheln.

Duchesse de Bourgogne (pronounced [dy.ˈʃɛs də buʁ.ɡɔɲ]) is a Flanders red ale-style beer produced by Brouwerij Verhaeghe in Vichte, Belgium. After a primary and secondary fermentation, this ale is matured in oak barrels for 18 months. The final product is a blend of a younger 8-month-old beer with an 18-month-old beer. The name of the beer is meant to honour Duchess Mary of Burgundy, the only daughter of Charles the Bold. She was born in Brussels in 1457, and died in a horse riding accident. Like all Flemish red ales, Duchesse de Bourgogne has a characteristically sour, fruity flavour similar to that of lambic beers.

https://en.wikipedia.org/

dark: true

carbonator : {
"backgroundColor": "#0b0b0b"
}

colors: {
"header": "#c8be8d",
"primary": "#c3af56"
}

Paco