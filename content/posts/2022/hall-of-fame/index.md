+++ 
draft = false
date = 2022-06-06T22:00:00+02:00
title = "2022 - to be continued.."
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["Hall of Fame"]
externalLink = ""
+++

<iframe 
    width="1280"
    height="720"
    src="https://www.youtube.com/embed/1wXcGsl0u2M"
    title="Hall of Fame 2022 - to be continued.."
    frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    allowfullscreen="">
</iframe>
