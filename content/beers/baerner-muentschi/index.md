+++ 
draft = false
date = 2019-01-01T22:00:00+02:00
title = "Bärner Müntschi"
description = "Luis's lieblings Bier"
slug = ""
authors = ["Jonas Burkhalter"]
categories = ["beer"]
externalLink = ""
+++

Das aktuelle Design der Webseite ist dem Bärner Müntschi gewidmet, das Lieblingsbier einer unserer Muscheln.

Prickelnd wie der erste Kuss. Leicht gehopft und unfiltriert. Das Bärner Müntschi ist das Lieblingsbier der Hauptstadt und weit über ihre Grenzen hinaus heiss begehrt.

https://felsenau.ch

dark: false

carbonator : {
"backgroundColor": "#ae6b02"
}

colors: {
"header": "#f7fbfe",
"primary": "#2e6874"
}

Luis